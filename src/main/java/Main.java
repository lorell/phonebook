import java.io.InputStreamReader;
import java.util.Set;
import javax.validation.Configuration;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.jalafoundation.dev37.phonebook.models.Address;

public class Main {

  private static Set<ConstraintViolation<Address>> violations;

  public static void main(String[] args) throws IllegalAccessException {
    String command = "app add –t contact –n newContact –c containerName –f";
    command = "app add –t contact –n newContact –c containerName –f";
    // Function
    // arguments //key => value
    Address address = new Address();
    address.setCity("Cochabamba");
    address.setCountry("Bolivia");

    Configuration<?> configuration = Validation.byDefaultProvider().configure();
    Validator validator = configuration.buildValidatorFactory().getValidator();
    violations = validator.validate(address);

    System.out.println(address.getCity());
    violations.stream().forEach(violation -> {
      System.out.println(violation.getMessage());
    });
  }
}