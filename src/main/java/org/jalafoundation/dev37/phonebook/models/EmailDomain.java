package org.jalafoundation.dev37.phonebook.models;

import java.util.Arrays;
import java.util.Optional;
import org.jalafoundation.dev37.phonebook.utils.StringUtils;

public enum EmailDomain {
  GMAIL("gmail.com"),
  OUTLOOK("outlook.com"),
  HOTMAIL("hotmail.com");

  private String domain;

  private EmailDomain(String value) {
    this.domain = StringUtils.AT_SIGN.concat(value);
  }

  public static final Optional<EmailDomain> getEmaiDomain(String value) {
    return Arrays.stream(EmailDomain.values())
        .filter(emailDomain -> emailDomain.domain.equals(value)).findFirst();
  }
}
