package org.jalafoundation.dev37.phonebook.models;

public enum ContactInfoType {
  EMAIL("email"),
  CELLPHONE("cellphone"),
  ADDRESS("address");

  private final String value;

  private ContactInfoType(String value) {
    this.value = value;
  }
}
