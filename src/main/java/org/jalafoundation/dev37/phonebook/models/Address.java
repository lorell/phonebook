package org.jalafoundation.dev37.phonebook.models;

import javax.validation.constraints.Email;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.jalafoundation.dev37.phonebook.annotations.ZipCode;
import org.jalafoundation.dev37.phonebook.utils.StringUtils;

@Setter
@Getter
@EqualsAndHashCode(exclude = {"city", "country"})
public class Address extends ContactInfo {
  private String streetAddress;
  private String city;

  private String country;
  @ZipCode
  private String zipCode;

  public Address() {
    super(ContactInfoType.ADDRESS);
  }

  public Address(String streetAddress) {
    this();
    this.streetAddress = streetAddress;
  }

  @ZipCode
  public void test(@ZipCode String text) {
    //Nothing by default
  }
  @Override
  protected void setValue() {
    this.value = new StringBuilder(this.streetAddress)
        .append(StringUtils.COMMA)
        .append(StringUtils.SPACE)
        .append(StringUtils.nullToBlank(this.city))
        .append(StringUtils.COMMA)
        .append(StringUtils.SPACE)
        .append(StringUtils.nullToBlank(this.country))
        .append(StringUtils.COMMA)
        .append(StringUtils.SPACE)
        .append(StringUtils.nullToBlank(this.zipCode))
        .toString();
  }

}
