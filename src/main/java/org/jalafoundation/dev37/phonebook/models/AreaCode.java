package org.jalafoundation.dev37.phonebook.models;

public enum AreaCode {
  BOLIVA("591");

  private final String value;
  private AreaCode(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
