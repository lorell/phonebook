package org.jalafoundation.dev37.phonebook.models;

import org.jalafoundation.dev37.phonebook.utils.StringUtils;

public class Cellphone extends ContactInfo {
  private AreaCode areaCode;

  private String number;

  public Cellphone() {
    super(ContactInfoType.CELLPHONE);
  }

  public Cellphone(AreaCode areaCode, String number) {
    this();
    this.areaCode = areaCode;
    this.number = number;
    setValue();
  }

  @Override
  protected void setValue() {
    this.value = new StringBuilder(this.areaCode.getValue())
        .append(StringUtils.SPACE)
        .append(this.number).toString();
  }
}
