package org.jalafoundation.dev37.phonebook.models;

import java.util.Objects;

public abstract class ContactInfo {
  private final ContactInfoType type;
  protected String value;
  private boolean isPreferred;

  public ContactInfo(ContactInfoType type) {
    this.type = type;
  }

  public ContactInfoType getType() {
    return type;
  }

  public boolean isPreferred() {
    return isPreferred;
  }

  public void setPreferred(boolean preferred) {
    isPreferred = preferred;
  }

  public String getValue() {
    return this.value;
  }

  protected abstract void setValue();

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContactInfo that = (ContactInfo) o;
    return type == that.type && value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, value);
  }
}
