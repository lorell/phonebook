package org.jalafoundation.dev37.phonebook.services;

import java.util.function.Predicate;
import org.jalafoundation.dev37.phonebook.models.Contact;

public class ContactPredicates {

  protected ContactPredicates() {
  }

  public static Predicate<Contact> findByAge(int age) {
    return contact -> contact.getAge() == age;
  }

  public static Predicate<Contact> findByName(String name) {
    return contact -> contact.getName().equals(name);
  }
}
