package org.jalafoundation.dev37.phonebook.services;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class ServiceUtils<T> {

  protected ServiceUtils() {
  }

  protected List<T> findAllBy(List<T> list, Predicate<T> predicate) {
    return list.stream().filter(predicate).toList();
  }

  protected Optional<T> findBy(List<T> list, Predicate<T> predicate) {
    return list.stream().filter(predicate).findFirst();
  }

}
