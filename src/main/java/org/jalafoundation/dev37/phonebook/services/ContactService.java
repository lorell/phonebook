package org.jalafoundation.dev37.phonebook.services;

import java.util.List;
import org.jalafoundation.dev37.phonebook.models.Contact;

public class ContactService extends ServiceUtils<Contact> {

  public List<Contact> findAllByAge(List<Contact> contacts, int age) {
    return super.findAllBy(contacts, ContactPredicates.findByAge(age));
  }

  public List<Contact> findAllByName(List<Contact> contacts, String name) {
    return super.findAllBy(contacts, ContactPredicates.findByName(name));
  }

}
