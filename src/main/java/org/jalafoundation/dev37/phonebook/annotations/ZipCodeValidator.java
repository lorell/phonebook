package org.jalafoundation.dev37.phonebook.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ZipCodeValidator implements ConstraintValidator<ZipCode, String> {

  @Override
  public void initialize(ZipCode constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
    if (s == null) {
      throw new NullPointerException("Zip code cannot null");
    }
    return s != null && (s.length() >3 && s.length() <= 5);
  }
}
