package org.jalafoundation.dev37.phonebook.utils;

public class StringUtils {
  public static final String AT_SIGN = "@";
  public static final String BLANK = "";
  public static final String COMMA = ",";
  public static final String SPACE = " ";

  private StringUtils() {
  }

  public static String nullToBlank(String text) {
    return text == null ? BLANK : text;
  }
}
